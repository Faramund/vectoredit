#ifndef RECTANGLEITEM_H
#define RECTANGLEITEM_H

#include <QGraphicsRectItem>

class RectangleItem : public QGraphicsRectItem
{
public:
    explicit RectangleItem(QColor fillingC, QColor frameC);
    void recalculateSize(QList<QPointF> drawingBuffer);
private:
    QPolygonF rectangle;
    QColor fillingColor;
    QColor frameColor;
};

#endif // RECTANGLEITEM_H
