#include "rectangleitem.h"

#include <QPainter>
#include <QDebug>

RectangleItem::RectangleItem(QColor fillingC, QColor frameC) : QGraphicsRectItem()
{
    fillingColor = fillingC;
    frameColor = frameC;
    setFlags(ItemIsSelectable | ItemIsMovable);
}

void RectangleItem::recalculateSize(QList<QPointF> drawingBuffer)
{
    QRectF rect(mapFromScene(drawingBuffer.first()), mapFromScene(drawingBuffer.last()));
    setPen(QPen(frameColor));
    setBrush(QBrush(fillingColor));
    setRect(rect);
    this->update(rect);
}
