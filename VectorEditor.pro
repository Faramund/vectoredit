#-------------------------------------------------
#
# Project created by QtCreator 2016-02-26T16:30:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VectorEditor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    rectangleitem.cpp \
    drawingscene.cpp

HEADERS  += mainwindow.h \
    rectangleitem.h \
    drawingscene.h
