#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QGraphicsScene;
class QGraphicsView;
class QButtonGroup;
class DrawingScene;
class QToolButton;
class QToolBox;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    QToolBox *toolBox;
    QButtonGroup* buttonGroup;
    QButtonGroup* colorGroup;
    QGraphicsView *view;
    DrawingScene *scene;
    void fillToolBox();

    QToolButton *frameColorButton;
    QToolButton *fillingColorButton;
signals:
    void colorChanged(int colorMode, QColor color);

private slots:
    void onToolSelect(int buttonNum);
    void onColorSelect(int buttonNum);
};

#endif // MAINWINDOW_H
