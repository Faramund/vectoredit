#include "mainwindow.h"
#include "drawingscene.h"

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QButtonGroup>
#include <QColorDialog>
#include <QVBoxLayout>
#include <QToolButton>
#include <QToolBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    fillToolBox();
    QVBoxLayout *toolsVLayout = new QVBoxLayout;
    toolsVLayout->addWidget(toolBox);
    toolsVLayout->addStretch();

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addLayout(toolsVLayout);


    QWidget *widget = new QWidget;
    widget->setLayout(layout);
    setCentralWidget(widget);

    scene = new DrawingScene(this);
    scene->setColorEdit(DrawingScene::FillingColor, QColor(Qt::green));
    scene->setColorEdit(DrawingScene::FrameColor,QColor(Qt::black));
    scene->setSceneRect(widget->rect());
    connect(this, SIGNAL(colorChanged(int,QColor)), scene, SLOT(setColorEdit(int,QColor)));

    view = new QGraphicsView(scene);
    layout->addWidget(view);
    showMaximized();
}

MainWindow::~MainWindow()
{

}

void MainWindow::fillToolBox()
{
    QToolButton *moveButton = new QToolButton;
    moveButton->setMinimumSize(QSize(120,20));
    moveButton->setText(tr("Перемешение"));
    moveButton->setCheckable(true);
    moveButton->setChecked(true);
    moveButton->setToolButtonStyle(Qt::ToolButtonTextOnly);

    QToolButton *pathButton = new QToolButton;
    pathButton->setMinimumSize(QSize(120,20));
    pathButton->setText(tr("Линия"));
    pathButton->setCheckable(true);
    pathButton->setToolButtonStyle(Qt::ToolButtonTextOnly);

    QToolButton *rectangleButton = new QToolButton;
    rectangleButton->setMinimumSize(QSize(120,20));
    rectangleButton->setText(tr("Прямоугольник"));
    rectangleButton->setCheckable(true);
    rectangleButton->setToolButtonStyle(Qt::ToolButtonTextOnly);


    QToolButton *deleteButton = new QToolButton;
    deleteButton->setMinimumSize(QSize(120,20));
    deleteButton->setText(tr("Удалить"));
    deleteButton->setToolButtonStyle(Qt::ToolButtonTextOnly);

    buttonGroup = new QButtonGroup;
    buttonGroup->addButton(pathButton,DrawingScene::InsertPath);
    buttonGroup->addButton(rectangleButton,DrawingScene::InsertRectangle);
    buttonGroup->addButton(moveButton, DrawingScene::MoveItem);
    buttonGroup->setExclusive(true);
    connect(buttonGroup, SIGNAL(buttonClicked(int)),this,SLOT(onToolSelect(int)));

    fillingColorButton = new QToolButton;
    fillingColorButton->setMinimumSize(QSize(60,20));
    fillingColorButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
    fillingColorButton->setPalette(QPalette(Qt::green));


    frameColorButton = new QToolButton;
    frameColorButton->setMinimumSize(QSize(60,20));
    frameColorButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
    frameColorButton->setPalette(QPalette(Qt::black));

    QHBoxLayout *fillingLayout = new QHBoxLayout;
    fillingLayout->addWidget(frameColorButton);
    fillingLayout->addWidget(fillingColorButton);

    colorGroup = new QButtonGroup;
    colorGroup->addButton(frameColorButton,DrawingScene::FrameColor);
    colorGroup->addButton(fillingColorButton,DrawingScene::FillingColor);
    connect(colorGroup, SIGNAL(buttonClicked(int)),this,SLOT(onColorSelect(int)));

    QVBoxLayout *toolsVLayout = new QVBoxLayout;
    toolsVLayout->addWidget(moveButton);
    toolsVLayout->addWidget(pathButton);
    toolsVLayout->addWidget(rectangleButton);
    toolsVLayout->addWidget(deleteButton);
    toolsVLayout->addLayout(fillingLayout);

    QWidget *toolsContWidget = new QWidget;
    toolsContWidget->setLayout(toolsVLayout);


    toolBox = new QToolBox;
    toolBox->setMinimumWidth(toolsContWidget->sizeHint().width());
    toolBox->setMinimumHeight(toolsContWidget->sizeHint().height());
    toolBox->addItem(toolsContWidget,tr("Инструменты"));
    toolBox->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum));
}

void MainWindow::onToolSelect(int buttonNum)
{
    switch (buttonNum) {
    case DrawingScene::InsertRectangle:
        scene->setMode(DrawingScene::InsertRectangle);
        break;
    case DrawingScene::InsertPath:
        scene->setMode(DrawingScene::InsertPath);
        break;
    case DrawingScene::MoveItem:
        scene->setMode(DrawingScene::MoveItem);
        break;
    default:
        break;
    }
}

void MainWindow::onColorSelect(int buttonNum)
{
    QColor color = QColorDialog::getColor();
    if (buttonNum == DrawingScene::FrameColor){
        frameColorButton->setPalette(QPalette(color));
    }
    else
    {
        fillingColorButton->setPalette(QPalette(color));
    }
    emit colorChanged(buttonNum,color);
}
