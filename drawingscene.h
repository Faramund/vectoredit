#ifndef DRAWINGSCENE_H
#define DRAWINGSCENE_H

#include <QGraphicsScene>

class RectangleItem;

class DrawingScene : public QGraphicsScene
{
    Q_OBJECT
public:
    enum Mode { InsertRectangle, InsertPath, MoveItem };
    enum ColorType { FillingColor, FrameColor};
    explicit DrawingScene(QObject *parent);
    void setMode(Mode mode);

public slots:
    void setColorEdit(int colorMode, QColor color);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) Q_DECL_OVERRIDE;
private:
    RectangleItem *nowCreateRect;
    QList <QPointF> drawingBuffer;
    Mode activeMode;

    QColor frameColor;
    QColor fillingColor;
};

#endif // DRAWINGSCENE_H
