#include "drawingscene.h"
#include "rectangleitem.h"

#include <QTextCursor>
#include <QGraphicsWidget>
#include <QGraphicsSceneMouseEvent>

DrawingScene::DrawingScene(QObject *parent) : QGraphicsScene(parent)
{
    setMode(DrawingScene::MoveItem);
}

void DrawingScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    switch (activeMode) {
    case InsertRectangle:
        nowCreateRect = new RectangleItem(fillingColor,frameColor);
        nowCreateRect->setPos(mouseEvent->scenePos());
        addItem(nowCreateRect);
        mouseEvent->accept();
        break;
    case InsertPath:
        break;
    default:
        break;
    }
    QGraphicsScene::mouseMoveEvent(mouseEvent);
}

void DrawingScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    switch (activeMode) {
    case InsertRectangle:
        drawingBuffer.append(mouseEvent->scenePos());
        nowCreateRect->recalculateSize(drawingBuffer);
        mouseEvent->accept();
        break;
    case InsertPath:
        break;
    default:
        break;
    }
    update();
    QGraphicsScene::mouseMoveEvent(mouseEvent);
}

void DrawingScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    switch (activeMode) {
    case InsertRectangle:
        if (itemsBoundingRect().width() > sceneRect().width() || itemsBoundingRect().height() > sceneRect().height())
            setSceneRect(itemsBoundingRect());
        mouseEvent->accept();
        break;
    case InsertPath:
        break;
    default:
        break;
    }
    drawingBuffer.clear();
    clearFocus();
    QGraphicsScene::mouseReleaseEvent(mouseEvent);
}

void DrawingScene::setMode(Mode mode)
{
    activeMode = mode;
}

void DrawingScene::setColorEdit(int colorMode, QColor color)
{
    switch (colorMode) {
    case FillingColor: fillingColor = color;
        break;
    case FrameColor: frameColor = color;
        break;
    default:
        break;
    }
}
